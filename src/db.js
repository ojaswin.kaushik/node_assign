const sqlite3 = require("sqlite3").verbose();
function open() {
  var db = new sqlite3.Database(
    "../db/cats.db",
    sqlite3.OPEN_READWRITE,
    (err) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log("Connected to the cats database.");
      }
    }
  );
  return db;
}
function insert(name, age, breed) {
  db = open();
  let insert_sql =
    "INSERT INTO CATS (Cat_Name , Cat_Age, Cat_Breed,id) Values(?,?,?,?)";
  db.run(
    insert_sql,
    [name, age, breed, Math.ceil(Math.random() * 1000)],
    (err) => {
      if (err) {
        console.log(err.message, "insert error");
      }
      console.log("A row has been added");
    }
  );
  close();
}

function update(name, age, breed, id) {
  db = open();
  //console.log("hi",id,name,age,breed)
  let update_sql =
    "UPDATE CATS SET Cat_Name = ?, Cat_Age = ?, Cat_Breed = ?  where id = ?";

  db.run(update_sql, [name, age, breed, id], (err) => {
    if (err) {
      return console.log(err.message, "update error");
    } else console.log("row has been updated");
  });
  close();
}
function Display_All() {
  db = open();
  let sql = "select * from CATS";

  db.each(sql, (err, row) => {
    if (err) {
      return console.log(err.message, "display error");
    }
    console.log(row.Cat_Name, row.Cat_Breed, row.Cat_Age, row.id);
  });

  close();
}
function Display_By_Id(id) {
  db = open();

  let sql = "select * from CATS where id = ?";
  db.get(sql, [id], (err, row) => {
    if (err) {
      return console.log("error in display by id");
    }
    if (!row) {
      return console.log("value does not exist");
    }
    //console.log(row.Cat_Name,row.Cat_Age,row.Cat_Breed)
  });
  close();
}
function Delete_Cat(id) {
  db = open();
  let delete_sql = "DELETE FROM CATS where id = ?";
  db.run(delete_sql, [id], (err) => {
    if (err) {
      return console.log(err.message, "delete error");
    }

    console.log("row deleted with the given id:", id);
  });
  close();
}
//update("wde",12,"weew",1)
//insert("hello",12,"second")
//Display_All()
//Delete_Cat("hello")
//Display_By_Id("some")
module.exports = {
  insert,
  update,
  Display_All,
  Display_By_Id,
  Delete_Cat,
};
function close() {
  db.close((err) => {
    if (err) {
      console.error(err.message);
    }
    console.log("Close the database connection.");
  });
}
