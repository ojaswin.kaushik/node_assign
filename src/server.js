const express = require('express')
const bodyParser =require('body-parser')
const app = express()
const port = 3000
const path =require('path')
const dir=__dirname
const dbs =require('./db.js')
//console.log(path,dir,"hello")
// GET / -> List of cats
// GET /:id -> Cat with given ID
// GET  /search?age_lte=10&age_gte=20 -> Returns cat with given age range
// PUT /:id -> Update cat with given ID with updated args
// POST / -> Create CAT
// DELETE /:id -> Delete Cat with given ID
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.get('/',async(req,res)=>
{
    
    //console.log(req.query)
    //console.log(req.params.uid[1])
    
    res.sendFile("index.html",{root:__dirname})
    dbs.Display_All()
})
app.delete('/:id',async(req,res)=>
{
    
    
    dbs.Delete_Cat (req.params.id[1])
    
})
app.get('/:id',(req,res)=>
{
    res.send("id display")
    //console.log(req.query)
    //console.log(req.params.id[1])
    dbs.Display_By_Id(Number(req.params.id[1]))
})

app.put('/:id',urlencodedParser,async(req,res)=>
{   
    console.log(req.query,"putting",req.body)
    dbs.update(req.body.Cat_Name,req.body.Age,req.body.Cat_Breed,Number(req.params.id[1]))
})

app.post('/',urlencodedParser,(req,res)=>
{
    
    dbs.insert(req.body.Cat_Name,req.body.Age,req.body.Cat_Breed)
    res.send("wow")

})



app.listen(port,()=>
{
    console.log("Server is running at",port.toString())
})